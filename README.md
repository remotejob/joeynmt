# joeynmt
https://github.com/joeynmt/joeynmt

kaggle datasets create -r tar -p data/

kaggle kernels push -p train/
kaggle kernels status sipvip/joeynmt

cp  output/my_model/48000.ckpt data/
cp  output/my_model/src_vocab.txt data/
cp  output/my_model/trg_vocab.txt data/

kaggle datasets version -r tar -p data/ -m "Updated data 0"

kaggle kernels output sipvip/joeynmt -p output/



cp /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/src-train.txt train.de.org
cp /home/juno/gowork/src/gitlab.com/remotejob/mltwiiter/data/tgt-train.txt train.en.org
cp /exwindoz/home/juno/repos/gitlab.com/remotejob/transformerml/dataoldbot/inputsentences.txt test.de

head -n 540000 train.de.org > train.de
head -n 540000 train.en.org > train.en

tail -n 35008 train.de.org > dev.de 
tail -n 35008 train.en.org > dev.en


25.07.2019

cp /exwindoz/home/juno/repos/gitlab.com/remotejob/transformerml/data/tweedoldbot.tsv data/

cut -f1 -d$'\t' data/tweedoldbot.tsv  > data/joeynmt/test/data/finbot/train.de.org 
cut -f2 -d$'\t' data/tweedoldbot.tsv  > data/joeynmt/test/data/finbot/train.en.org

cd data/finbot/data
head -n 109000 train.ask.org > train.ask
head -n 109000 train.ans.org > train.ans

tail -n 599 train.ask.org > dev.ask
tail -n 599 train.ans.org > dev.ans


-------------------------------
paste -d"\t" data/finbot/data/test.de output/my_model/00005000.hyps.test > test.txt
sed -i G test.txt
sed -i 's/\t/\n/' test.txt
sed -i 's/^\s*$/--------------/' test.txt 

paste -d"\t" data/finbot/data/dev.de output/my_model/00005000.hyps.dev > dev.txt
sed -i G dev.txt
sed -i 's/\t/\n/' dev.txt
sed -i 's/^\s*$/--------------/' dev.txt 


