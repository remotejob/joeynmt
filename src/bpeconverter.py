import os

from bpemb import BPEmb
bpemb_fi = BPEmb(lang="fi", dim=50)


trainaskfile ='data/finbot/data/train.ask'
trainaskfilebpe ='data/finbot/data/trainbpe.ask'

trainansfile ='data/finbot/data/train.ans'
trainansfilebpe ='data/finbot/data/trainbpe.ans'

devaskfile ='data/finbot/data/dev.ask'
devaskfilebpe ='data/finbot/data/devbpe.ask'

devansfile ='data/finbot/data/dev.ans'
devansfilebpe ='data/finbot/data/devbpe.ans'


testfile ='data/finbot/data/test.ask'
testfilebpe ='data/finbot/data/testbpe.ask'

def convert(infile,outfile):

    if os.path.exists(outfile):
        os.remove(outfile)


    with open(infile) as fp:
        line = fp.readline()

        while line:

            bpeenc = bpemb_fi.encode(line.strip())
            bpeline =' '.join(bpeenc)
            with open(testfilebpe, "a") as outfile:
                outfile.write(bpeline+'\n')

        line = fp.readline()

convert(trainaskfile,trainaskfilebpe)
convert(trainansfile,trainansfilebpe)

convert(devaskfile,devaskfilebpe)
convert(devansfile,devansfilebpe)

convert(testfile,testfilebpe)








